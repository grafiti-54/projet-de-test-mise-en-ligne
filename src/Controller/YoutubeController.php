<?php

namespace App\Controller;

use App\Entity\Youtube;
use App\Form\YoutubeType;
use App\Repository\YoutubeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class YoutubeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(Request $request, EntityManagerInterface $em, YoutubeRepository $youtubeRepository): Response
    {
        //On récupére l'entité Youtube
        $youtube = new Youtube();

        //On crée le formulaire qui va nous permettre d'ajouter un nouveau lien, on récupére l'objet youtube
        $form = $this->createForm(YoutubeType::class, $youtube);

        //On récupére les données du formulaire
        $form->handleRequest($request);

        //on vérifie si le formulaire est valide
        if ($form->isSubmitted() && $form->isValid()) {
            //on rempli l'entité youtube avec les données du formulaire
            $youtube = $form->getData();

            $em->persist($youtube);
            $em->flush();

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);

        }

        return $this->renderForm('youtube/index.html.twig', [
            'form' => $form,
            'youtubes' => $youtubeRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_video')]
    public function video(Youtube $youtube): Response
    {
        return $this->render('youtube/video.html.twig', [
            'name' => $youtube->getName(),
            'url' => $youtube->getUrl(),
        ]);
    }
}
